% +------------------------------------------------------------------------------------------+
% |   Filename           : main_distance_perfect_conflict.m                                                   |
% |   Description        : Test script to calculate Hazard zone volume.                      |
% |                         RPAS intruder engaged ownship under perfect conflict conditions. |
% |   Created by         : M. P?rez-Batlle                                                    |
% |   Created on         : 05 October 2020                                                    |
% |   Date last modified : 05 October 2020                                                    |
% +------------------------------------------------------------------------------------------+

%clear all
%close all

% Set hazard zone variables 
dmod = 4000 * 0.3048; %as DMOD in RTCA 260A: 4000 ft
taumod = 35; %as TMOD in RTCA 260A: 35s
hmd = dmod; %as in RTCA 260A: HMD = DMOD
zthr = 450 * 0.3048; %as VMOD in TCA 260A 450 ft

%set g
T_MAX = 2*60; % Maximum simulation time

% Set initial variables (hold position variables (x_manned, y_manned, z_manned, x_rpas, y_rpas, z_rpas) to 
% zero in order to ensure perfect collision
x_manned = 0; 
y_manned = 0;
z_manned = 0;

x_rpas = 0;
y_rpas = 0;
z_rpas = 0;


modv_rpas = 120 * 0.51444;  % RPAS speed in m/s
modv_own = 90 * 0.51444;    % Ownship speed in m/s
zodot= 0;

polarplot(0,0); % Set empty polar plot
rlim([0,5]);    % Adjust radius dimension in NM
hold on


for t = 1:5:T_MAX % For each simulation time
    for theta = 0: 0.1 : 2*pi % For each conflict angle 
        % Place intruder in its initial horizontal position (according to time and speed)
        si=[x_rpas - t*modv_rpas*sin(theta),y_rpas - t*modv_rpas*cos(theta)]; 
        % Set intruder altitude
        szi = z_rpas; 
        % Place ownship in its initial horizontal position (according to time and speed)
        so=[x_manned - t*modv_own*sin(0),y_manned - t*modv_own*cos(0)];
        % Set intruder altitude
        szo=z_manned;
        % Set ownship speed (always the same regardless theta)
        vo =[0, modv_own];
        % Set intruder speed (depends on theta
        vi =[modv_rpas*sin(theta),modv_rpas*cos(theta)];
    
        % Call check hazard function
        result = check_hazard(dmod, taumod, hmd, zthr, si, szi, vi, so, szo, vo );
        % Add result to plot. If result = 1 intruder is violating ownship's hazard
        % Zone. result = 0 otherwise.
        if result == 1
            % Plot a cross point in red in the particular position
            polarplot(theta, norm(si-so)/1852, 'x', 'color', 'black')%norm(si-so) return the relative distance
        else
            % Plot a circle point in green in the particular position
            polarplot(theta, norm(si-so)/1852, 'o', 'color', 'green')%norm(si-so) return the relative distance
        end
    end
end


