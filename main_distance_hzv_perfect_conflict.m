params = init_params();

%% Personalize scenario
% NMAC horizontal distance 500 ft
params.scenario.nmac_hor = 500 * 0.3048;    
% Set hazard zone variables 
% Default values given in RTCA 260A
params.scenario.wc.dmod = 4000 * 0.3048; %as DMOD in RTCA 260A: 4000 ft
params.scenario.wc.taumod = 35; %as TMOD in RTCA 260A: 35s
params.scenario.wc.hmd = params.scenario.wc.dmod; %as in RTCA 260A: HMD = DMOD
params.scenario.wc.zthr =450 * 0.3048; %as VMOD in TCA 260A 450 ft
% Maximum simulation time [s]
params.scenario.t_cpa_max = 100;            
% Time step [s]
params.scenario.delta_t = 1 ;  
params.misc.dt = params.scenario.delta_t;
params.misc.t_max = params.scenario.t_cpa_max;
% Avoidance turn angle step [deg]
params.trajectory.delta_h = 30* pi/180;   
% Maximum avoidance turn angle [deg]
params.trajectory.delta_h_max = 90* pi/180; 


%% Personalize aircraft dynamics
% RPAS speed in m/s
params.ac2.v = 100 * 0.51444; 
% Own speed in m/s
params.ac1.v = 150 * 0.51444; 
% RPAS system delay in seconds
params.ac2.delays.system = 0;   
% RPAS roll rate delay in seconds
params.ac2.delays.turn = 0;    
% Target turn rate for avoidance 3deg/sec
params.ac2.omega = 3.0 * pi/180;


distance_hzv_perfect_conflict(params)