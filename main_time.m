clear all


dmod = 4000 * 0.3048; %as DMOD in RTCA 260A: 4000 ft
taumod = 35; %as TMOD in RTCA 260A: 35s
hmd = dmod;
zthr =450 * 0.3048; %as VMOD in TCA 260A 450 ft


T_MAX = 2*60; %2 min
xo = 0; 
yo = 0;
zo = 0;

xi = 0;
yi = 0;
zi = 0;


modvi = 60 * 0.51444;
modvo = 200 * 0.51444;
zodot= 0;



polarplot(0,0);
hold on
for t = 1:10:T_MAX
    i=1;
    for theta = 0: 0.05 : 2*pi
        si=[xi - t*modvi*sin(theta),yi - t*modvi*cos(theta)];
        szi = zi;
        so=[xo - t*modvo*sin(0),yo - t*modvo*cos(0)];
        szo=zo;
        arg = (si(1)-so(1))/(si(2)-so(2));
        vo =[0, modvo];
        vi =[modvi*sin(arg),modvi*cos(arg)];

        result = check_hazard(dmod, taumod, hmd, zthr, si, szi, vi, so, szo, vo );
        if result == 1
            polarplot(theta, t, 'x', 'color', 'red')
        else
            polarplot(theta, t, 'o', 'color', 'green')
        end
    end
end


