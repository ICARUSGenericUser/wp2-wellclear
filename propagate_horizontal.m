function [ac1, ac2] = propagate_horizontal(params,ac1, ac2, curr_delta_h, delay)
            t = 0:params.misc.dt:params.misc.t_max;
            %Allocate matrices for the aircraft positions
            ac1.pos_left = zeros(length(t),3);
            ac1.pos_right = zeros(length(t),3);
            ac2.pos_right = zeros(length(t),3);
            ac2.pos_left = zeros(length(t),3);
%             ac1.v_left = zeros(length(t),3);
%             ac1.v_right = zeros(length(t),3);
%             ac2.v_left = zeros(length(t),3);
%             ac2.v_right = zeros(length(t),3);
            
            ac1.pos_left(:,3) = params.enc.h;
            ac1.pos_right(:,3) = params.enc.h;
            ac2.pos_left(:,3) = params.enc.h;
            ac2.pos_right(:,3) = params.enc.h;
            

            %loop to calculate aircraft positions
            tstop_ac2 = 1;
            for l=1:1:length(t)
                %if ac1 is intended to turn
                
                if params.ac1.turns == 1
                    %if ac1 is actually turning
                    if params.ac1.omega*t(l) < curr_delta_h
                        ac1.pos_left(l,1) = ac1.init(1) + ac1.R*cos(3/2*pi + params.ac1.omega*t(l));
                        ac1.pos_left(l,2) = ac1.init(2) + ac1.R + ac1.R*sin(3/2*pi + params.ac1.omega*t(l));
%                         ac1.v_left(l,1) = curr_ac1_v * cos(params.ac1.omega*t(l));
%                         ac1.v_left(l,2) = curr_ac1_v * sin(params.ac1.omega*t(l));
                        ac1.pos_right(l,1) = ac1.pos_left(l,1);
                        ac1.pos_right(l,2) = -ac1.pos_left(l,2); %negatiu perque gira en sentit contrari
%                         ac1.v_right(l,1) = ac1.v_left(l,1);
%                         ac1.v_right(l,2) = -ac1.v_left(l,2); %negatiu perque gira en sentit contrari
                        tstop_ac1 = t(l);
                        %if uav has ended its turn
                    else
                        ac1.pos_left(l,1) = ac1.init(1) + ac1.R*cos(3/2*pi + params.ac1.omega*tstop_ac1) + params.ac1.v*(t(l)-tstop_ac1)*cos(params.ac1.delta_h);
                        ac1.pos_left(l,2) = ac1.init(2) + ac1.R + ac1.R*sin(3/2*pi + params.ac1.omega*tstop_ac1) + params.ac1.v*(t(l)-tstop_ac1)*sin(params.ac1.delta_h);
%                         ac1.v_left(l,1) = curr_ac1_v * cos(curr_ac1_delta_h);
%                         ac1.v_left(l,2) = curr_ac1_v * sin(curr_ac1_delta_h);
                        ac1.pos_right(l,1) = ac1.pos_left(l,1);
                        ac1.pos_right(l,2) = -ac1.pos_left(l,2);
%                         ac1.v_right(l,1) = ac1.v_left(l,1);
%                         ac1.v_right(l,2) = -ac1.v_left(l,2);
                    end
                %if uav is not intended to turn
                else
                    ac1.pos_left(l,1) = ac1.init(1) + params.ac1.v*t(l);
                    ac1.pos_left(l,2) = 0;
%                     ac1.v_left(l,1) = curr_ac1_v;
%                     ac1.v_left(l,2) = 0;
                    ac1.pos_right(l,1) = ac1.pos_left(l,1);
                    ac1.pos_right(l,2) = -ac1.pos_left(l,2);
%                     ac1.v_right(l,1) = curr_ac1_v;
%                     ac1.v_right(l,2) = 0;
                end
                if l < delay
                    ac2.pos_left(l,1) = ac2.init(1,1) + params.ac2.v*t(l);
                    ac2.pos_left(l,2) = 0;
                    ac2.pos_right(l,1) = ac2.pos_left(l,1);
                    ac2.pos_right(l,2) = -ac2.pos_left(l,2);
                    continue
                end
                %if intruded is intended to turn
                if params.ac2.turns == 1
                    %if intruder is actually turning
                    
                    if params.ac2.omega*t(l) < curr_delta_h
                        ac2.pos_left(l,1) = ac2.init(1) + ac2.R*cos(3/2*pi + params.ac2.omega*t(l));
                        ac2.pos_left(l,2) = ac2.init(2) + ac2.R + ac2.R*sin(3/2*pi + params.ac2.omega*t(l));
%                         ac2.v_left(l,1) = curr_ac2_v * cos(params.ac2.omega*t(l));
%                         ac2.v_left(l,2) = curr_ac2_v * sin(params.ac2.omega*t(l));
                        ac2.pos_right(l,1) = ac2.pos_left(l,1);
                        ac2.pos_right(l,2) = -ac2.pos_left(l,2);
%                         ac2.v_right(l,1) = ac2.v_left(l,1);
%                         ac2.v_right(l,2) = -ac2.v_left(l,2);
                        tstop_ac2 = t(l);
                        %if intruder has ended its turn
                    else
                        ac2.pos_left(l,1) = ac2.init(1) + ac2.R*cos(3/2*pi + params.ac2.omega*tstop_ac2) + params.ac2.v*(t(l)-tstop_ac2)*cos(curr_delta_h);
                        ac2.pos_left(l,2) = ac2.init(2) + ac2.R + ac2.R*sin(3/2*pi + params.ac2.omega*tstop_ac2) + params.ac2.v*(t(l)-tstop_ac2)*sin(curr_delta_h);
%                         ac2.v_left(l,1) = curr_ac2_v * cos(params.ac2.delta_h);
%                         ac2.v_left(l,2) = curr_ac2_v * sin(params.ac2.delta_h);
                        ac2.pos_right(l,1) = ac2.pos_left(l,1);
                        ac2.pos_right(l,2) = -ac2.pos_left(l,2);
%                         ac2.v_right(l,1) = ac2.v_left(l,1);
%                         ac2.v_right(l,2) = -ac2.v_left(l,2);
                    end
                       %if intruder is not intended to turn
                else
                    ac2.pos_left(l,1) = ac2.init(1,1) + params.ac2.v*t(l);
                    ac2.pos_left(l,2) = 0;
                    ac2.pos_right(l,1) = ac2.pos_left(l,1);
                    ac2.pos_right(l,2) = -ac2.pos_left(l,2);
%                     ac2.v_left(l,1) = params.ac2.v;
%                     ac2.v_left(l,2) = 0;
%                     ac2.v_right(l,1) = params.ac2.v;
%                     ac2.v_right(l,2) = 0;
                end
            end
end