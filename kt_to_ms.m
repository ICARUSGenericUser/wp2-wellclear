function [ms] = kt_to_ms(kt)
    ms = kt * 1852 / 3600;
end