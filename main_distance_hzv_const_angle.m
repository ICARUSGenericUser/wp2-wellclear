clear all
close all

params = init_params();

%if true debug aircraft positions
params.misc.debug = 0;


%% Personalize scenario
% NMAC horizontal distance 500 ft
params.scenario.nmac_hor = 500 * 0.3048;    
% Set hazard zone variables 
% Default values given in RTCA 260A
params.scenario.wc.dmod = 4000 * 0.3048; %as DMOD in RTCA 260A: 4000 ft
params.scenario.wc.taumod = 35; %as TMOD in RTCA 260A: 35s
params.scenario.wc.hmd = params.scenario.wc.dmod; %as in RTCA 260A: HMD = DMOD
params.scenario.wc.zthr =450 * 0.3048; %as VMOD in TCA 260A 450 ft
% Encounter relative angle theta
% 180 degrees indicates frontal conflict, 0 degrees indicates takeover conflict
params.scenario.theta = 120 * pi / 180;
% Maximum simulation time [s]
params.scenario.t_cpa_max = 100;            
% Minimum simulation time [s]
params.scenario.t_cpa_min = 0;
% Maxim simulation lateral distance [ft]
params.scenario.d_offset_max = 15000*0.3048;   
% Time step [s]
params.scenario.delta_t = 2;  
params.misc.dt = params.scenario.delta_t;
params.misc.t_max = params.scenario.t_cpa_max;
% Lateral off step from perfect collision [ft]
params.scenario.delta_d = 200*0.3048;   
% Avoidance turn angle step [deg]
params.trajectory.delta_h = 30* pi/180;   
% Maximum avoidance turn angle [deg]
params.trajectory.delta_h_max = 90* pi/180; 

%CPA altitude offset [ft]
params.scenario.z_offset= ft_to_m(0);


%% Personalize aircraft dynamics
% RPAS speed in m/s
params.ac2.v = 20 * 0.51444; 
% Own speed in m/s
params.ac1.v = 150 * 0.51444; 
% RPAS system delay in seconds
params.ac2.delays.system = 0;   
% RPAS roll rate delay in seconds
params.ac2.delays.turn = 0;    
% Target turn rate for avoidance 3deg/sec
params.ac2.omega = 3.0 * pi/180;
%RPAS ROCD in feet/min
params.ac2.rocd=fpm_to_ms(500);


%function call
name = char(sprintf("distance_hzv_const_angle_%d_%d_%d", round(params.ac1.v/0.51444), round(params.ac2.v/0.51444), round(params.scenario.theta*180.0/pi)));
distance_hzv_const_angle(params, name);