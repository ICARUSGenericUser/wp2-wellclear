% +----------------------------------------------------------------------+
% |   Filename           : main_distance.m                               |
% |   Description        : test script to calculate Hazard zone volume   |
% |   Created by         : M. P�rez-Batlle                               |
% |   Created on         : 05 October 2020                               |
% |   Date last modified : 05 October 2020                               |
% +----------------------------------------------------------------------+

clear all
close all

%if trrue debug aircraft positions
debug = 0;

%Set hazard zone variables 
dmod = 4000 * 0.3048; %as DMOD in RTCA 260A: 4000 ft
taumod = 35; %as TMOD in RTCA 260A: 35s
hmd = dmod; %as in RTCA 260A: HMD = DMOD
zthr =450 * 0.3048; %as VMOD in TCA 260A 450 ft

%set theta
theta = 120 * pi / 180;

%set maximum simlution time
T_MAX = 2*120; %maximum simulation time
D_MAX = 6000*0.3048;
delta_t = 10;
delta_d = 100*0.3048; %paral�lel distance from perfect collision 

%set initial variables (hold position variables (x_manned, y_manned, z_manned, x_rpas, y_rpas, z_rpas) to 
%zero in order to ensure perfect collision
x_manned = 0; 
y_manned = 0;
z_manned = 0;

x_rpas = 0;
y_rpas = 0;
z_rpas = 0;


modv_rpas = 100 * 0.51444; %rpas speed in m/s
modv_manned = 250 * 0.51444; %manned speed in m/s
zodot= 0;

%polarplot(0,0);%set empty polar plot
%rlim([0,5]); %adjust radius dimension in NM
plot(0,0);

if debug == 1
    axis('equal') 
end
hold on

for t = 1:delta_t:T_MAX %for each simulation time
    %place intruder in its initial horizontal position (according to
    %time and speed)
    si_o=[x_rpas - t*modv_rpas*sin(theta),y_rpas - t*modv_rpas*cos(theta)]; 
    for d =0:delta_d:D_MAX
        %add a lateral offset
        si=[x_rpas - t*modv_rpas*sin(theta) + d*sin(theta-pi/2),y_rpas - t*modv_rpas*cos(theta)+ d*cos(theta-pi/2)]; %offset to the left
        si_2=[x_rpas - t*modv_rpas*sin(theta) - d*sin(theta-pi/2),y_rpas - t*modv_rpas*cos(theta)- d*cos(theta-pi/2)]; %offset to the right
        %set intruder altitude
        szi = z_rpas; 
        %place ownship in its initial horizontal position (according to time and speed)
        so=[x_manned - t*modv_manned*sin(0),y_manned - t*modv_manned*cos(0)];
        %set intruder altitude
        szo=z_manned;
        %set ownship speed (always the same regardless theta)
        vo =[0, modv_manned];
        %set intruder speed (depends on theta
        vi =[modv_rpas*sin(theta),modv_rpas*cos(theta)];
    
        %call check hazard function (both simmetries)
        result = check_hazard(dmod, taumod, hmd, zthr, si, szi, vi, so, szo, vo );
        result_2 = check_hazard(dmod, taumod, hmd, zthr, si_2, szi, vi, so, szo, vo );
        %add result to plot. If result = 1 intruder is violating ownship's hazard
        %zone. result = 0 otherwise.
        if result == 1
            %plot a cross point in red in the particular position
            if debug == 0
                plot(norm(si_o-so)/1852,-d, 'x', 'color','red')
            else
                plot(si(1),si(2), 'o', 'color','blue' );
                plot(si_o(1),si_o(2), '*', 'color', 'green' );
                plot(so(1),so(2), 'x', 'color', 'red');
            end
            
        else
            %plot a circle point in green in the particular position
            if debug == 0
                plot(norm(si_o-so)/1852,-d, 'o', 'color','green')
            else
                plot(si(1),si(2), 'o', 'color','blue' );
                plot(si_o(1),si_o(2), '*', 'color', 'green' );
                plot(so(1),so(2), 'x', 'color', 'red');
            end
        end
        if result_2 == 1
            %plot a cross point in red in the particular position
            if debug == 0
                plot(norm(si_o-so)/1852, d, 'x', 'color','red')
            else
                plot(si_2(1),si_2(2), 'o', 'color', 'black');
                plot(si_o(1),si_o(2), '*', 'color', 'green' );
                plot(so(1),so(2), 'color', 'red');
            end
        else
            %plot a circle point in green in the particular position
            if debug == 0
                plot(norm(si_o-so)/1852, d, 'o', 'color','green')
            else
                plot(si_2(1),si_2(2), 'o', 'color', 'black');
                plot(si_o(1),si_o(2), '*', 'color', 'green' );
                plot(so(1),so(2), 'color', 'red');
            end
        end
    end
end


