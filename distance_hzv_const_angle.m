% +------------------------------------------------------------------------------------+
% |   Filename           : main_distance_hzv_const_angle.m                              |
% |   Description        : Test script to calculate Hazard Zone Volume.
% |                        Intruders follow a constant angle and constant
% |                        speed trajectory. Avoidance maneuver is then applied under
% |                        limited turn rate and initial latency to determine if 
% |                        NMAC is violated.
% |   Created by         : M. P?rez-Batlle                                              |
% |   Created on         : 05 October 2020                                              |
% |   Date last modified : 19 October 2020                                              |
% +------------------------------------------------------------------------------------+

function distance_hzv_const_angle(params, name)

% Set initial variables (hold position variables (x_manned, y_manned, z_manned, x_rpas, y_rpas, z_rpas) to 
% zero in order to ensure perfect collision.
x_manned = 0; 
y_manned = 0;
z_manned = 0;

x_rpas = 0;
y_rpas = 0;
z_rpas = 0;

zodot= 0;

delay = params.ac2.delays.system + params.ac2.delays.turn;

%polarplot(0,0);%set empty polar plot

% f1 = figure('visible','off');
% f1 = figure;
% plot(0,0);
% hold on
% axis('equal') 

if params.misc.debug == 1
    t_init = 0;
else
    t_init = params.scenario.t_cpa_min;
end

ac1.R=params.ac1.v/params.ac1.omega; 
ac2.R=params.ac2.v/params.ac2.omega;

R_0= [cos(pi/2),-sin(pi/2), 0; sin(pi/2),cos(pi/2),0; 0, 0, 1];
R= [cos(params.scenario.theta),-sin(params.scenario.theta), 0; sin(params.scenario.theta),cos(params.scenario.theta),0; 0, 0, 1];

i= 1;
n=1;

si_o = zeros( (params.scenario.t_cpa_max - t_init)/params.scenario.delta_t, 3 );
si = zeros( (params.scenario.t_cpa_max - t_init)/params.scenario.delta_t, round(params.scenario.d_offset_max/params.scenario.delta_d),3);
si_2 = zeros( (params.scenario.t_cpa_max - t_init)/params.scenario.delta_t, round(params.scenario.d_offset_max/params.scenario.delta_d),3);
so = zeros( (params.scenario.t_cpa_max - t_init)/params.scenario.delta_t, round(params.scenario.d_offset_max/params.scenario.delta_d),3);
min_dists = zeros ( params.trajectory.delta_h_max / params.trajectory.delta_h,2);

pos_x = zeros ((params.scenario.t_cpa_max - t_init)/params.scenario.delta_t * round(params.scenario.d_offset_max/params.scenario.delta_d*2) ,1);
pos_y = zeros ((params.scenario.t_cpa_max - t_init)/params.scenario.delta_t * round(params.scenario.d_offset_max/params.scenario.delta_d*2) ,1);
val = zeros ((params.scenario.t_cpa_max - t_init)/params.scenario.delta_t * round(params.scenario.d_offset_max/params.scenario.delta_d*2) ,1);


for t = t_init:params.scenario.delta_t:params.scenario.t_cpa_max % For each simulation time

    % Place intruder in its initial horizontal position (according to
    % time and speed)
    si_o(i,:)=[x_rpas - t*params.ac2.v*sin(params.scenario.theta),y_rpas - t*params.ac2.v*cos(params.scenario.theta), -t*params.ac2.rocd]; 

    ac1.init(1,1) = -params.ac1.v*t;
    ac1.init(1,2) = 0;
%     ac1.init(1,3) = -params.ac1.rocd*t;
    ac2.init(1,1) = -params.ac2.v*t;
    ac2.init(1,2) = 0;
%     ac2.init(1,3) = -params.ac2.rocd*t;

%     plot(ac2.pos_left(:,1),ac2.pos_left(:,2),'color','red');
    j = 1;
    for d =0:params.scenario.delta_d:params.scenario.d_offset_max
%         k = 1;
%         for z =-params.scenario.z_offset_max:params.scenario.delta_z:params.scenario.z_offset_max
        l=1;
        for h = 0:params.trajectory.delta_h:params.trajectory.delta_h_max
            [ac1, ac2] = propagate_horizontal(params,ac1,ac2, h, delay);
            ac1 = rotate(ac1, -R_0 );
            ac2 = rotate(ac2, -R_0 );
%     plot(ac1.pos_left(:,1),ac1.pos_left(:,2),'color','blue');
            ac2 = rotate(ac2, R);
            ac2_2 = ac2;
            ac2_init = ac2;
        % Add a lateral offset, both sides computed at the same time
            si(i,j,:)=[x_rpas - t*params.ac2.v*sin(params.scenario.theta) + d*sin(params.scenario.theta-pi/2),y_rpas - t*params.ac2.v*cos(params.scenario.theta)+ d*cos(params.scenario.theta-pi/2), z_rpas - t*params.ac2.rocd + params.scenario.z_offset]; %offset to the left
            si_2(i,j,:)=[x_rpas - t*params.ac2.v*sin(params.scenario.theta) - d*sin(params.scenario.theta-pi/2),y_rpas - t*params.ac2.v*cos(params.scenario.theta)- d*cos(params.scenario.theta-pi/2), z_rpas - t*params.ac2.rocd + params.scenario.z_offset]; %offset to the right

            ac2.pos_left(:,1) = ac2_init.pos_left(:,1)+d*sin(params.scenario.theta-pi/2);
            ac2.pos_left(:,2) = ac2_init.pos_left(:,2)+d*cos(params.scenario.theta-pi/2);
            ac2.pos_right(:,1) = ac2_init.pos_right(:,1)+d*sin(params.scenario.theta-pi/2);
            ac2.pos_right(:,2) = ac2_init.pos_right(:,2)+d*cos(params.scenario.theta-pi/2);
            ac2_2.pos_left(:,1) = ac2_init.pos_left(:,1)-d*sin(params.scenario.theta-pi/2);
            ac2_2.pos_left(:,2) = ac2_init.pos_left(:,2)-d*cos(params.scenario.theta-pi/2);
            ac2_2.pos_right(:,1) = ac2_init.pos_right(:,1)-d*sin(params.scenario.theta-pi/2);
            ac2_2.pos_right(:,2) = ac2_init.pos_right(:,2)-d*cos(params.scenario.theta-pi/2);

%            plot(si(i,j,1),si(i,j,2),'o');
%            plot(ac2.pos_left(:,1),ac2.pos_left(:,2),'color','blue');
%            plot(ac2.pos_right(:,1),ac2.pos_right(:,2),'color','red');
%            plot(si_2(i,j,1),si_2(i,j,2),'o');
%            plot(ac2_2.pos_right(:,1),ac2_2.pos_right(:,2),'color','red');

            % Check if within NMAC
            dist_1 = sqrt((ac2.pos_right(:,1) - ac1.pos_left(:,1)).^2 + (ac2.pos_right(:,2) - ac1.pos_left(:,2)).^2);
            dist_2 = sqrt((ac2.pos_left(:,1) - ac1.pos_left(:,1)).^2 + (ac2.pos_left(:,2) - ac1.pos_left(:,2)).^2);
            min_dist_1 = min(dist_1);
            min_dist_2 = min(dist_2);
            min_dists(l,1) = max(min_dist_1, min_dist_2);
            dist_1 = sqrt((ac2_2.pos_right(:,1) - ac1.pos_left(:,1)).^2 + (ac2_2.pos_right(:,2) - ac1.pos_left(:,2)).^2);
            dist_2 = sqrt((ac2_2.pos_left(:,1) - ac1.pos_left(:,1)).^2 + (ac2_2.pos_left(:,2) - ac1.pos_left(:,2)).^2);
            min_dist_1 = min(dist_1);
            min_dist_2 = min(dist_2);
            min_dists(l,2) = max(min_dist_1, min_dist_2);
            l=l+1;
        end

        %place ownship in its initial horizontal position (according to time and speed)
        so(i,j,:)=[x_manned - t*params.ac1.v*sin(0),y_manned - t*params.ac1.v*cos(0), z_manned - t*params.ac1.rocd];

        min_dist = max(min_dists(:,1));
        if min_dist < params.scenario.nmac_hor && (abs(si(i,j,3) - so(i,j,3))) < params.scenario.nmac_vert
            nmac_violated_1 = 1;
        else
            nmac_violated_1 = 0;
        end


        min_dist = max(min_dists(:,2));
        if min_dist < params.scenario.nmac_hor && (abs(si_2(i,j,3) - so(i,j,3))) < params.scenario.nmac_vert
            nmac_violated_2 = 1;
        else
            nmac_violated_2 = 0;
        end



        %set ownship speed (always the same regardless theta)
        vo =[0, params.ac1.v];
        %set intruder speed (depends on theta)
        vi =[params.ac2.v*sin(params.scenario.theta),params.ac2.v*cos(params.scenario.theta)];

        % Call check hazard function (both simmetries)
        si_curr = [si(i,j,1), si(i,j,2)];
        si_2_curr = [si_2(i,j,1), si_2(i,j,2)];
        so_curr = [so(i,j,1), so(i,j,2)];
        szi_curr = si(i,j,3);
        szi_2_curr = si_2(i,j,3);
        szo_curr = so(i,j,3);
        result = check_hazard(params.scenario.wc.dmod, params.scenario.wc.taumod, params.scenario.wc.hmd, params.scenario.wc.zthr, si_curr, szi_curr, vi, so_curr, szo_curr, vo );
        result_2 = check_hazard(params.scenario.wc.dmod, params.scenario.wc.taumod, params.scenario.wc.hmd, params.scenario.wc.zthr, si_2_curr, szi_2_curr, vi, so_curr, szo_curr, vo );

        pos_x(n) = (si(i,j,1)-so(i,j,1))/1852;
        pos_y(n) = (si(i,j,2)-so(i,j,2))/1852;
        pos_z(n) = (si(i,j,3)-so(i,j,3))/1852;
        pos_x(n+1) = (si_2(i,j,1)-so(i,j,1))/1852;
        pos_y(n+1) = (si_2(i,j,2)-so(i,j,2))/1852;
        pos_z(n+1) = (si_2(i,j,3)-so(i,j,3))/1852;

        % Add result to plot. If result = 1 intruder is violating ownship's hazard
        % zone. result = 0 otherwise.
        if result == 1
            % Plot a cross point in red in the particular position
            if params.misc.debug == 0
                if nmac_violated_1 == 1
                    val(n)=1;
%                       plot3((si(i,j,1)-so(i,j,1))/1852,(si(i,j,2)-so(i,j,2))/1852, (si(i,j,3) - so(i,j,3))/0.3048,'x', 'color','red');
                else
                    val(n)=2;
%                       plot3((si(i,j,1)-so(i,j,1))/1852,(si(i,j,2)-so(i,j,2))/1852,(si(i,j,3) - so(i,j,3))/0.3048,'x', 'color','black');
                end
                hold on
            else
                plot(si(i,j,1)/1852,si(i,j,2)/1852, 'o', 'color','blue' );
                plot(si_o(i,1)/1852,si_o(i,2)/1852, '*', 'color', 'green' );
                plot(so(i,j,1)/1852,so(i,j,2)/1852, 'x', 'color', 'red');
            end

        else
            % Plot a circle point in green in the particular position
            if params.misc.debug == 0
                if nmac_violated_1 == 1
                    val(n)=1;
%                       plot3((si(i,j,1)-so(i,j,1))/1852,(si(i,j,2)-so(i,j,2))/1852,(si(i,j,3) - so(i,j,3))/0.3048,'o', 'color','red');
                else
                    val(n)=3;
%                       plot3((si(i,j,1)-so(i,j,1))/1852,(si(i,j,2)-so(i,j,2))/1852,(si(i,j,3) - so(i,j,3))/0.3048,'o', 'color','green');
                end
                hold on
            else
                plot(si(i,j,1)/1852,si(i,j,2)/1852, 'o', 'color','blue' );
                plot(si_o(i,1)/1852,si_o(i,2)/1852, '*', 'color', 'green' );
                plot(so(i,j,1)/1852,so(i,j,2)/1852, 'x', 'color', 'red');
            end
        end
        if result_2 == 1
            % Plot a cross point in red in the particular position
            if params.misc.debug == 0
                if nmac_violated_2 == 1
                 val(n+1)=1;
%                       plot3((si_2(i,j,1)-so(i,j,1))/1852,(si_2(i,j,2)-so(i,j,2))/1852,(si_2(i,j,3) - so(i,j,3))/0.3048,'x', 'color','red');
                else
                    val(n+1)=2;
%                       plot3((si_2(i,j,1)-so(i,j,1))/1852,(si_2(i,j,2)-so(i,j,2))/1852,(si_2(i,j,3) - so(i,j,3))/0.3048,'x', 'color','black');
                end
                hold on
            else
                plot(si_2(i,j,1)/1852,si_2(i,j,2)/1852, 'o', 'color', 'black');
                plot(si_o(i,1)/1852,si_o(i,2)/1852, '*', 'color', 'green' );
                plot(so(i,j,1)/1852,so(i,j,2)/1852, 'color', 'red');
            end
        else
            % Plot a circle point in green in the particular position
            if params.misc.debug == 0
                if nmac_violated_2 == 1
                    val(n+1)=1;
%                      plot3((si_2(i,j,1)-so(i,j,1))/1852,(si_2(i,j,2)-so(i,j,2))/1852,(si_2(i,j,3) - so(i,j,3))/0.3048,'o', 'color','red');
                else
                    
                    val(n+1)=3;
%                       plot3((si_2(i,j,1)-so(i,j,1))/1852,(si_2(i,j,2)-so(i,j,2))/1852,(si_2(i,j,3) - so(i,j,3))/0.3048,'o', 'color','green');
                end
                hold on
            else
                plot(si_2(i,j,1)/1852,si_2(i,j,2)/1852, 'o', 'color', 'black');
                plot(si_o(i,1)/1852,si_o(i,2)/1852, '*', 'color', 'green' );
                plot(so(i,j,1)/1852,so(i,j,2)/1852, 'color', 'red');
            end
            
           
        end
        n=n+2;
        j = j + 1;
    end
    i = i+1;
end

xv = linspace(min(pos_x), max(pos_x), 200);
yv = linspace(min(pos_y), max(pos_y), 200);
[X,Y] = meshgrid(xv, yv);
Z = griddata(pos_x,pos_y,val,X,Y);

view(0, 90);

 % Draw ownship and intruder headings
 plot(0,0,'ob');
 hold on
 plot([0,0], [0,1],'-b');
 plot(1,1, 'ob');
 plot([1,1+sin(params.scenario.theta)],[1,1+cos(params.scenario.theta)], '-b');
 tit=sprintf("HAZ volume for speeds %d kts (ownship) vs %d kts (RPAS)\nEncounter angle %d",round(params.ac1.v/0.51444),round(params.ac2.v/0.51444),round(params.scenario.theta*(180/pi)));
 title(tit);
 axis('equal') 

figure
surf(X, Y, Z);
view(0, 90);

figure
contour(X,Y,Z,2);
view(0, 90);

% % Draw NMAC circle around ownship
% t_circ =0:0.05:6.28;
% R = params.scenario.nmac_hor / 1852;
% x1_circ = (R*cos(t_circ))';
% y1_circ= (R*sin(t_circ))';
% plot(x1_circ,y1_circ, 'LineWidth',2, 'color','red');
% 
% % Limit drawing area
% xlim([-3,1]);
% ylim([-1,3]);
% 

% saveas(f1,name,'png');

end






