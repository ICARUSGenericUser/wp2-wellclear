% +------------------------------------------------------------------------------------------+
% |   Filename           : main_distance_perfect_conflict.m                                                   |
% |   Description        : Test script to calculate Hazard zone volume.                      |
% |                         RPAS intruder engaged ownship under perfect conflict conditions. |
% |   Created by         : M. P?rez-Batlle                                                    |
% |   Created on         : 05 October 2020                                                    |
% |   Date last modified : 05 October 2020                                                    |
% +------------------------------------------------------------------------------------------+

function distance_hzv_perfect_conflict(params)

% Set initial variables (hold position variables (x_manned, y_manned, z_manned, x_rpas, y_rpas, z_rpas) to 
% zero in order to ensure perfect collision
x_manned = 0; 
y_manned = 0;
z_manned = 0;

x_rpas = 0;
y_rpas = 0;
z_rpas = 0;


zodot= 0;

delay = params.ac2.delays.system + params.ac2.delays.turn;


polarplot(0,0); % Set empty polar plot
rlim([0,5]);    % Adjust radius dimension in NM
hold on

ac1.R=params.ac1.v/params.ac1.omega; 
ac2.R=params.ac2.v/params.ac2.omega;

R_0= [cos(pi/2),-sin(pi/2), 0; sin(pi/2),cos(pi/2),0; 0, 0, 1];

i = 1;
for t = 1:params.scenario.delta_t:params.scenario.t_cpa_max % For each simulation time
    ac1.init(1,1) = -params.ac1.v*t;
    ac1.init(1,2) = 0;
    ac2.init(1,1) = -params.ac2.v*t;
    ac2.init(1,2) = 0;
    j = 1;
    for theta = 0: 0.1 : 2*pi % For each conflict angle 
        R= [cos(theta),-sin(theta), 0; sin(theta),cos(theta),0; 0, 0, 1];
        k = 1;
        for h = 0:params.trajectory.delta_h:params.trajectory.delta_h_max
            [ac1, ac2] = propagate_horizontal(params,ac1,ac2, h, delay);
            ac1 = rotate(ac1, -R_0 );
            ac2 = rotate(ac2, -R_0 );
            ac2 = rotate(ac2, R);
       
            % Place intruder in its initial horizontal position (according to time and speed)
            si=[x_rpas - t*params.ac2.v*sin(theta),y_rpas - t*params.ac2.v*cos(theta)]; 
            % Set intruder altitude
            szi = z_rpas; 
            
            dist_1 = sqrt((ac2.pos_right(:,1) - ac1.pos_left(:,1)).^2 + (ac2.pos_right(:,2) - ac1.pos_left(:,2)).^2);
            dist_2 = sqrt((ac2.pos_left(:,1) - ac1.pos_left(:,1)).^2 + (ac2.pos_left(:,2) - ac1.pos_left(:,2)).^2);
            min_dist_1 = min(dist_1);
            min_dist_2 = min(dist_2);
            min_dists(k) = max(min_dist_1, min_dist_2);
            k =k+1;
        end
        min_dist = max(min_dists(:));
        if min_dist < params.scenario.nmac_hor
            nmac_violated_1 = 1;
        else
            nmac_violated_1 = 0;
        end

        % Place ownship in its initial horizontal position (according to time and speed)
        so=[x_manned - t*params.ac1.v*sin(0),y_manned - t*params.ac1.v*cos(0)];
        % Set intruder altitude
        szo=z_manned;
        % Set ownship speed (always the same regardless theta)
        vo =[0, params.ac1.v];
        % Set intruder speed (depends on theta
        vi =[params.ac2.v*sin(theta),params.ac2.v*cos(theta)];

        % Call check hazard function
        result = check_hazard(params.scenario.wc.dmod, params.scenario.wc.taumod, params.scenario.wc.hmd, params.scenario.wc.zthr, si, szi, vi, so, szo, vo );
        % Add result to plot. If result = 1 intruder is violating ownship's hazard
        % Zone. result = 0 otherwise.
        if result == 1
            % Plot a cross point in red in the particular position
            if nmac_violated_1 == 1
                polarplot(theta, norm(si-so)/1852, 'x', 'color', 'red')%norm(si-so) return the relative distance
            else
                polarplot(theta, norm(si-so)/1852, 'x', 'color', 'black')%norm(si-so) return the relative distance
            end
        else
            if nmac_violated_1 == 1
                polarplot(theta, norm(si-so)/1852, 'o', 'color', 'red')%norm(si-so) return the relative distance
            else
                % Plot a circle point in green in the particular position
                polarplot(theta, norm(si-so)/1852, 'o', 'color', 'green')%norm(si-so) return the relative distance
            end
        end
        j=j+1;
    end
    i=i+1;
end


