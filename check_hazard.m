function [ ok ] = check_hazard(dmod, taumod, hmd, zthr, si, szi, vi, so, szo, vo )
%CHECK_HAZARD Summary of this function goes here
%   Detailed explanation goes here

v = vi-vo;
s = si-so;
r = norm(s);
rdot = dot(s,v)/norm(s);
cond1 = r <= max(dmod, 0.5*sqrt((rdot*taumod)^2+ 4 * dmod^2)-rdot*taumod);
tcpa = max(0,-dot(s,v)/norm(v)^2);
hmdp = sqrt((s(1) + v(1)*tcpa)^2 + (s(2) + v(2)*tcpa)^2);
cond2 = hmdp <= hmd;
dh = abs(szi - szo);
cond3 = dh <= zthr;
ok = cond1 && cond2 && cond3;

end




