function [ ok ] = check_nonhazard(params, si, szi, vi, vzi, so, szo, vo, vzo )
%CHECK_HAZARD Summary of this function goes here
%   Detailed explanation goes here

v = vi-vo;
s = si-so;
r = norm(s);
rdot = dot(s,v)/norm(s);
cond1 = r > max(params.wc.dmod, 0.5*sqrt((rdot*params.wc.taumod)^2+ 4 * params.wc.dmod^2)-rdot*params.wc.taumod);
tcpa = max(0,-dot(s,v)/norm(v)^2);
hmdp = sqrt((s(1) + v(1)*tcpa)^2 + (s(2) + v(2)*tcpa)^2);
cond2 = hmdp > params.wc.hmd;
dh = abs(szi - szo);
vz = vzi-vzo;
V = max(params.wc.zthr, params.wc.zthr - vz*params.wc.taumod);
cond3 = dh > V;
ok = cond1 && cond2 && cond3;

end

