% +----------------------------------------------------------------------+
% |   Filename           : main_distance.m                               |
% |   Description        : test script to calculate Hazard zone volume   |
% |   Created by         : M. P�rez-Batlle                               |
% |   Created on         : 05 October 2020                               |
% |   Date last modified : 05 October 2020                               |
% +----------------------------------------------------------------------+

clear all
close all

%if true debug aircraft positions
debug = 0;

%Set hazard zone variables 
dmod = 4000 * 0.3048; %as DMOD in RTCA 260A: 4000 ft
taumod = 35; %as TMOD in RTCA 260A: 35s
hmd = dmod; %as in RTCA 260A: HMD = DMOD
zthr =450 * 0.3048; %as VMOD in TCA 260A 450 ft

%set theta
theta = 135* pi / 180;

%set maximum simlution time
T_MAX = 300; %maximum simulation time
D_MAX = 30000*0.3048;
delta_t = 20;
delta_d = 1000*0.3048; %paral�lel distance from perfect collision 

%set initial variables (hold position variables (x_manned, y_manned, z_manned, x_rpas, y_rpas, z_rpas) to 
%zero in order to ensure perfect collision
x_manned = 0; 
y_manned = 0;
z_manned = 0;

x_rpas = 0;
y_rpas = 0;
z_rpas = 0;


modv_rpas = 200 * 0.51444; %rpas speed in m/s
modv_manned = 140 * 0.51444; %manned speed in m/s
zodot= 0;

%polarplot(0,0);%set empty polar plot
%rlim([0,5]); %adjust radius dimension in NM
plot(0,0);
hold on
axis('equal') 

if debug == 1
    t_init = 0;
else
    t_init = -T_MAX;
end

i= 1;
j = 1;

for t = t_init:delta_t:T_MAX %for each simulation time
    %place intruder in its initial horizontal position (according to
    %time and speed)
    si_o(i,:)=[x_rpas - t*modv_rpas*sin(theta),y_rpas - t*modv_rpas*cos(theta)]; 
    i = i+1;
    for d =0:delta_d:D_MAX
        %add a lateral offset
        si(i,j,:)=[x_rpas - t*modv_rpas*sin(theta) + d*sin(theta-pi/2),y_rpas - t*modv_rpas*cos(theta)+ d*cos(theta-pi/2)]; %offset to the left
        si_2(i,j,:)=[x_rpas - t*modv_rpas*sin(theta) - d*sin(theta-pi/2),y_rpas - t*modv_rpas*cos(theta)- d*cos(theta-pi/2)]; %offset to the right
        %set intruder altitude
        szi = z_rpas; 
        %place ownship in its initial horizontal position (according to time and speed)
        so(i,j,:)=[x_manned - t*modv_manned*sin(0),y_manned - t*modv_manned*cos(0)];
        %set intruder altitude
        szo=z_manned;
        %set ownship speed (always the same regardless theta)
        vo =[0, modv_manned];
        %set intruder speed (depends on theta
        vi =[modv_rpas*sin(theta),modv_rpas*cos(theta)];
    
        %call check hazard function (both simmetries)
        si_curr = [si(i,j,1), si(i,j,2)];
        si_2_curr = [si(i,j,1), si(i,j,2)];
        so_curr = [so(i,j,1), so(i,j,2)];
        result = check_hazard(dmod, taumod, hmd, zthr, si_curr, szi, vi, so_curr, szo, vo );
        result_2 = check_hazard(dmod, taumod, hmd, zthr, si_2_curr, szi, vi, so_curr, szo, vo );
        %add result to plot. If result = 1 intruder is violating ownship's hazard
        %zone. result = 0 otherwise.
        if result == 1
            %plot a cross point in red in the particular position
            if debug == 0
                plot((si(i,j,1)-so(i,j,1))/1852,(si(i,j,2)-so(i,j,2))/1852,'x', 'color','red');
            else
                plot(si(i,j,1),si(i,j,2), 'o', 'color','blue' );
                plot(si_o(i,1),si_o(i,2), '*', 'color', 'green' );
                plot(so(i,j,1),so(i,j,2), 'x', 'color', 'red');
            end
            
        else
            %plot a circle point in green in the particular position
            if debug == 0
                plot((si(i,j,1)-so(i,j,1))/1852,(si(i,j,2)-so(i,j,2))/1852,'o', 'color','green');
            else
                plot(si(1),si(2), 'o', 'color','blue' );
                plot(si_o(i,1),si_o(i,2), '*', 'color', 'green' );
                plot(so(i,j,1),so(i,j,2), 'x', 'color', 'red');
            end
        end
        if result_2 == 1
            %plot a cross point in red in the particular position
            if debug == 0
                 plot((si_2(i,j,1)-so(1))/1852,(si_2(i,j,2)-so(i,j, 2))/1852,'x', 'color','red');
            else
                plot(si_2(i,j,1),si_2(i,j,2), 'o', 'color', 'black');
                plot(si_o(i,1),si_o(i,2), '*', 'color', 'green' );
                plot(so(i,j,1),so(i,j,2), 'color', 'red');
            end
        else
            %plot a circle point in green in the particular position
            if debug == 0
                 plot((si_2(i,j,1)-so(i,j,1))/1852,(si_2(i,j,2)-so(i,j,2))/1852,'o', 'color','green');
            else
                plot(si_2(i,j,1),si_2(i,j,2), 'o', 'color', 'black');
                plot(si_o(i,1),si_o(i,2), '*', 'color', 'green' );
                plot(so(i,j,1),so(i,j,2), 'color', 'red');
            end
        end
        j = j + 1;
    end
end





