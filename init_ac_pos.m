function [ ac1, ac2 ] = init_ac_pos(params, ac1, ac2, curr_ac1_v, curr_t_pca )
%INIT_AC_POS Summary of this function goes here
%   Detailed explanation goes here
    ac1.init(1,1) = -curr_ac1_v*curr_t_pca;
    ac1.init(1,2) = 0;
    ac2.init(1,1) = -params.ac2.v*curr_t_pca;
    ac2.init(1,2) =0;
end

