function [params] = init_params()

%% Atmosphere parameters
params.atmos.deltaT = 0;

%% Encounter paremeters
params.ac1.turns = 0; %is aircraft 1 turning? [0: not turning, 1: turning]
params.ac2.turns = 1; %is aircraft 2 turning? [0: not turning, 1: turning]
params.ac1.vertical= 0; %is aircraft 1 turning? [0: not turning, 1: turning]
params.ac2.vertical = 0; %is aircraft 2 turning? [0: not turning, 1: turning]
params.ac1.omega = 6; % turn rate for aircraft 1 [deg/sec]
params.ac2.omega = 6; % turn rate for aircraft 2 [deg/sec]
params.ac1.v = [80]; % airspeed for aircraft 1 [KTAS]
params.ac2.v = 200; % airspeed for aircraft 1 [KTAS]
params.ac1.delta_h = 90; % considereheadingd heading changes [deg]
params.ac2.delta_h = 90; % considered  changes [deg]
params.ac1.apm = 'rq2';
params.ac2.apm = 'rq2';
params.ac1.phase = 'climb';
params.ac2.phase = 'descent';
params.ac1.mass = 191; %mass of aircraft 1 [kg]
params.ac2.mass = 191; %mass of aircraft 2 [kg]
params.ac1.operation = 'CNT_CAS'; %speed schedule [CNT_CAS only so far]
params.ac2.operation = 'CNT_CAS'; %speed schedule [CNT_CAS only so far]
params.ac1.delays.system = 0; %ac1 system delay [s]
params.ac2.delays.system = 0; %ac2 system delay [s]
params.ac1.delays.turn = 0; %ac1 turn delay [s]
params.ac2.delays.turn = 0; %ac2 turn delay [s]
params.ac1.rocd = 0; % ac1 rate of climb/descent [ft/min]
params.ac2.rocd = 1000; % ac2 rate of climb/descent [ft/min]


params.ac1.altitude = 5000;
params.ac2.altitude = 5000;
params.enc.h = params.ac1.altitude; %encounter altitude [ft]
params.scenario.nmac_hor = 500; %target horizontal distance at pca [ft] (500 ft)
params.scenario.nmac_vert = 100; %target vertical distance at pca [ft]
params.scenario.theta = 180; %encounter angle [deg]
params.scenario.t_cpa_min = -75; %maximum considered time to CPA [s]
params.scenario.t_cpa_max = 100; %maximum considered time to CPA [s]
params.scenario.d_offset_max = 10000; %maximum considered parallel offset [ft]
params.scenario.delta_t = 10; %delta time to CPA [s]
params.scenario.delta_d = 100; %delta parallel offset [ft]
params.trajectory.delta_h = 30; %delta change of heading [deg]
params.trajectory.delta_h_max = 90; %maximum change of heading [deg]
params.scenario.z_offset = 1000;%altitude offset [ft]

params.res.vertical = 0; %vertical resolution [0: disabled, 1: enabled]
params.res.horizontal = 1; %horizontal resolution [0: disabled, 1: enabled]
if params.res.horizontal == 0
    params.ac1.turns = 0; 
    params.ac2.turns = 0; 
    params.ac1.delta_h = 0;
    params.ac2.delta_h = 0;
end

params.misc.dt = 2;
params.misc.t_max = 130; %maximum trajectory time [s]
params.misc.t_pca = 1:2:params.misc.t_max; %time to conflict [s]
params.misc.delta_beta = 5; %conflict angle steps [deg]
params.misc.beta_vect=0:params.misc.delta_beta:360;
params.misc.debug = 0;
params.misc.daa = 1;
params.misc.ca = 1;

%% Misc parameters
params.prefix_name = '';
params.scenario_name = '';

%% Transform into SI units
params.ac1.omega = deg_to_rad(params.ac1.omega);
params.ac2.omega = deg_to_rad(params.ac2.omega);
params.ac1.v = kt_to_ms(params.ac1.v);
params.ac2.v = kt_to_ms(params.ac2.v);
params.ac1.delta_h = deg_to_rad(params.ac1.delta_h); 
params.ac2.delta_h = deg_to_rad(params.ac2.delta_h); 
params.scenario.nmac_hor = ft_to_m(params.scenario.nmac_hor);
params.scenario.nmac_vert = ft_to_m(params.scenario.nmac_vert);
params.ac1.altitude = ft_to_m(params.ac1.altitude);
params.ac2.altitude = ft_to_m(params.ac1.altitude);
params.enc.h = params.ac1.altitude; %force encounter altitude [ft] to ac1 altitude
params.scenario.theta = deg_to_rad(params.scenario.theta);
params.scenario.d_offset_max = ft_to_m(params.scenario.d_offset_max);
params.scenario.delta_d = ft_to_m(params.scenario.delta_d);
params.scenario.z_offset = ft_to_m(params.scenario.z_offset);
params.trajectory.delta_h = deg_to_rad(params.trajectory.delta_h);
params.trajectory.delta_h_max = deg_to_rad(params.trajectory.delta_h_max);
params.ac1.rocd = fpm_to_ms(params.ac1.rocd);
params.ac2.rocd = fpm_to_ms(params.ac2.rocd);




%% TCAS parameters
params.tcass(1).sl = 3;
params.tcass(1).tau_ta = 25;
params.tcass(1).tau_ra = 15;
params.tcass(1).dmod_ta= nm_to_m(0.33);
params.tcass(1).dmod_ra= nm_to_m(0.2);
params.tcass(1).zthr = ft_to_m(600);
params.tcass(1).alim = ft_to_m(300);
params.tcass(1).hmd = ft_to_m(1215);

params.tcass(2).sl = 4;
params.tcass(2).tau_ta = 30;
params.tcass(2).tau_ra = 20;
params.tcass(2).dmod_ta= nm_to_m(0.48);
params.tcass(2).dmod_ra= nm_to_m(0.35);
params.tcass(2).zthr = ft_to_m(600);
params.tcass(2).alim = ft_to_m(300);
params.tcass(2).hmd = ft_to_m(2126);

params.tcass(3).sl = 5;
params.tcass(3).tau_ta = 40;
params.tcass(3).tau_ra = 25;
params.tcass(3).dmod_ta= nm_to_m(0.75);
params.tcass(3).dmod_ra= nm_to_m(0.55);
params.tcass(3).zthr = ft_to_m(600);
params.tcass(3).alim = ft_to_m(350);
params.tcass(3).hmd = ft_to_m(3342);

params.tcass(4).sl = 6;
params.tcass(4).tau_ta = 45;
params.tcass(4).tau_ra = 30;
params.tcass(4).dmod_ta= nm_to_m(1);
params.tcass(4).dmod_ra= nm_to_m(0.8);
params.tcass(4).zthr = ft_to_m(600);
params.tcass(4).alim = ft_to_m(400);
params.tcass(4).hmd = ft_to_m(4861);

params.tcass(5).sl = 7;
params.tcass(5).tau_ta = 48;
params.tcass(5).tau_ra = 35;
params.tcass(5).dmod_ta= nm_to_m(1.3);
params.tcass(5).dmod_ra = nm_to_m(1.1);
params.tcass(5).zthr = ft_to_m(700);
params.tcass(5).alim = ft_to_m(600);
params.tcass(5).hmd = ft_to_m(6683);

params.tcass(6).sl = 7;
params.tcass(6).tau_ta = 48;
params.tcass(6).tau_ra = 35;
params.tcass(6).dmod_ta= nm_to_m(1.3);
params.tcass(6).dmod_ra= nm_to_m(1.1);
params.tcass(6).zthr = ft_to_m(800);
params.tcass(6).alim = ft_to_m(700);
params.tcass(6).hmd = ft_to_m(6683);

%% Well clear parameters
params.scenario.wc.dmod = 4000; %ft
params.scenario.wc.hmd = 4000; %ft
params.scenario.wc.zthr = 450; %ft
params.scenario.wc.taumod = 35; %s
params.scenario.wc.tcoa = 0; %s
params.scenario.wc.mata = 55;%s
params.scenario.wc.preventive.thrlate = 20;%s
params.scenario.wc.preventive.threarly = 75;%s
params.scenario.wc.corrective.thrlate = 20;%s
params.scenario.wc.corrective.threarly = 75;%s
params.scenario.wc.warning.thrlate = 15;%s
params.scenario.wc.warning.threarly = 55;%s


%% Transform to SI units
params.scenario.wc.dmod = ft_to_m(params.scenario.wc.dmod);
params.scenario.wc.hmd = ft_to_m(params.scenario.wc.hmd);
params.scenario.wc.zthr = ft_to_m(params.scenario.wc.zthr);
end

